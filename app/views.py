from flask import render_template
from app import app

import json

some_data = {
    'key': 'value',
    'key2': 'value2'
}

@app.route('/')
def main():
    return render_template('index.html', data=some_data)
