# usage

```
pipenv --python 3.7
pipenv install
pipenv run gunicorn -b0.0.0.0:8000 wsgi:app
```

## Deploy to openshift

```
oc new-app https://gitlab.com/modle13/base-flask.git
oc expose svc/base-flask
```
